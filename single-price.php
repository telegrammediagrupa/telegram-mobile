<?php
get_header(); ?>

<?php
if( have_posts() ) {
    while ( have_posts() ) {
        the_post();
        ?>

        <div class="single-page longform">

        <?php
        // Check article format
        $format = intval(get_post_meta( get_the_ID(), 'telegram_expanded', true ));

        // Naslov izvan slike
        if( $format == 2 ) { ?>

            <div class="single-head out-picture">
	        <?php
	        $video = get_post_meta($post->ID, 'video', true);
	        if ($video) {
		        ?>
                <div class="thumb">
			        <?php
			        echo apply_filters('widget_text_content', trim($video)); ?>
                </div>
		        <?php
	        }
	        else { ?>
                <div class="thumb">
                    <?php
                    the_post_thumbnail('single_v1'); ?>
                </div>
            <?php } ?>
                <br>
                <div class="titles">
                    <div class="container">
                        <div class="head-meta">
                            <?php
                            $coauthors = get_coauthors();
                            $has_authors = false;
                            if ($coauthors){
                                echo 'Piše: ';
                                $i = 0;

                                foreach ( $coauthors as $author ) {
                                    ?>
                                    <a href="<?php echo get_author_posts_url( $author->ID, $author->user_login ); ?>"><?php echo $author->display_name;
                                        if ( sizeof( $coauthors ) > 1 && sizeof( $coauthors ) < $i + 1 ) {
                                            echo ', ';
                                        }
                                        ?></a>
                                    <?php
                                    $i ++;
                                }
                                $has_authors = true;
                            }
                            if ( telegram_get_photographer() ) {
	                            if ($has_authors) {
		                            echo ' | Snima: ';
	                            }
	                            else {
		                            echo ' Snima: ';
	                            }
	                            echo telegram_get_photographer();
                            }
                            ?>
                        </div>

                        <h1 class="title">
                            <?php the_title(); ?>
                        </h1>
                        <h2 class="subtitle">
                            <?php
                            echo get_post_meta( get_the_ID(), 'subtitle', true ) ; ?>
                        </h2>
                    </div>
                </div>
            </div>

        <?php }

        // Bez slike
        else if( $format == 4 ) { ?>
            <div class="single-head no-img">
                <div class="container titles">
                    <h3 class="overtitle"><?php
	                    if (get_post_meta( get_the_ID(), 'nadnaslov', true ) ) {
		                    echo get_post_meta( get_the_ID(), 'nadnaslov', true ) ;
	                    }
	                    else {
		                    the_category();
	                    }
	                    ?></h3>
                    <h1 class="title">
                        <?php the_title(); ?>
                    </h1>
                    <h2 class="subtitle">
                        <?php
                        echo get_post_meta( get_the_ID(), 'subtitle', true ) ; ?>
                    </h2>
                    <div class="article-meta">
                        <?php
                        foreach (get_coauthors() as $author) {
                            $image = coauthors_get_avatar($author, 150);
                            if ($image) {
                            ?>
                            <div class="author-thumb">
                                <?php echo $image ?>
                            </div>
                                <?php } ?>
                            <span class="author">
                                Piše:
                                <a href="<?php echo get_author_posts_url( $author->ID, $author->user_login ); ?>">
                                    <?php echo $author->display_name; ?>
                                </a>
                            </span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } else { ?>

            <div class="single-head in-picture">
                <div class="thumb">
                    <?php
                    the_post_thumbnail('mobile-head'); ?>

                    <div class="container titles">
                        <h3 class="overtitle"><?php
	                        if (get_post_meta( get_the_ID(), 'nadnaslov', true ) ) {
		                        echo get_post_meta( get_the_ID(), 'nadnaslov', true ) ;
	                        }
	                        else {
		                        the_category();
	                        }
	                        ?></h3>
                        <h1 class="title">
                            <?php the_title(); ?>
                        </h1>
                        <h2 class="subtitle">
                            <?php
                            echo get_post_meta( get_the_ID(), 'subtitle', true ) ; ?>
                        </h2>
                        <div class="article-meta">
                            <?php
                            foreach (get_coauthors() as $author) { ?>
                                <div class="author-thumb">
                                    <?php echo coauthors_get_avatar($author, 150); ?>
                                </div>
                                <span class="author">
                                Piše:
                                    <a href="<?php echo get_author_posts_url( $author->ID, $author->user_login ); ?>">
                                        <?php echo $author->display_name; ?>
                                    </a>
                                </span>
                                <span class="photo-author">
                                    <?php
                                    // The Photographer
                                    if( telegram_get_photographer() ) { ?>
                                        <div class="photographer">
                                            Foto: <?php echo telegram_get_photographer() ?>
                                        </div>
                                    <?php
                                    }  ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>

        <?php } ?>

        <div class="single-body container">
            <!-- Single Content -->
            <div class="single-content">
                <div class="content-container">

                    <?php
                    if( get_post_meta( get_the_ID(), 'perex', true )  ) {
                        echo '<div class="perex">'.get_post_meta( get_the_ID(), 'perex', true ) .'</div>';
                    }

                    if (has_tag('koronavirus')) {
	                    the_widget( 'Telegram_Corona_News' );
                    }

                    ?>

                    <div class="article-meta">
                        <span>
                            ZADNJA IZMJENA:
                        </span>
                        <span class="time">
                            <?php echo date_i18n( get_option(  'date_format' ), get_the_modified_time( 'U' ) ) ?>
                        </span>
                        <span class="rcmds">
                            <?php echo intval(get_post_meta(get_the_ID(), '_recommendations', true)) ?> preporuka
                        </span>
                        <span class="comms">
                            <?php echo intval(get_post_meta(get_the_ID(), '_comments', true)) ?> komentara
                        </span>
                    </div>

                    <div class="the-content">
                        <?php
                        // Article content
                        the_content(); ?>
                    </div>
                    <div class="ender">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tg-logo-red.svg"/>
                    </div>
                    <?php
                    // Article tags
                    the_tags('<div class="tags">', ', ', '</div>'); ?>

                    <?php // Article bottom sidebar
                    dynamic_sidebar('sidebar-single-bottom-1'); ?>

                </div>
            </div>

        </div>
        <div class="navigation">
            <?php previous_post_link(); ?>
            <?php next_post_link(); ?>
        </div>
        <div class="container">
            <?php
            // Bottom single sidebar - Fullwidth
            dynamic_sidebar('mobile-sidebar-single-bottom-2'); ?>
        </div>

        <?php
    }
}
?>
    </div>

<?php
get_footer();
