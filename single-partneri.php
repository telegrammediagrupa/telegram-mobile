<?php
get_header(); ?>

<?php
if( have_posts() ) {
    while ( have_posts() ) {
        the_post();
        ?>

        <div class="single-page container">

        <div class="single-head">
            <h3 class="overtitle">TELEGRAM PARTNERI</h3>
            <h1 class="title">
                <?php the_title(); ?>
            </h1>
            <h2 class="subtitle">
                <?php
                echo get_excerpt( get_post_meta( get_the_ID(), 'subtitle', true ) , 141 ); ?>
            </h2>
            <div class="article-meta">
                    <div class="author-block">
                        <div class="author-thumb" style="background: none;">
                            <img class="partner-logo" height="100" src="<?php echo get_post(get_post_meta( get_the_ID(), 'logo_partner', true ))->guid ; ?>"/>
                        </div>
                        <span class="author">
                            <a href="#">
                            U suradnji s <?php echo get_post_meta( get_the_ID(), 'partner', true ) ; ?>
                            </a>
                        </span>
                    </div>
            </div>
        </div>

        <div class="single-body">
            <!-- Single Content -->
            <div class="single-content">
                <div class="thumb">
                    <?php
                    // 840x530 px
                    the_post_thumbnail('large');
                    ?>
                    <?php
                    // The Photographer
                    if( telegram_get_photographer() ) { ?>
                        <div class="photographer">
                            Foto: <?php echo telegram_get_photographer() ?>
                        </div>
                        <?php
                    }  ?>
                </div>

                <div class="content-container">
                    <div class="article-meta">
                        <span>
                            ZADNJA IZMJENA:
                        </span>
                        <span class="time">
                            <?php echo date_i18n( get_option(  'date_format' ), get_the_modified_time( 'U' ) ) ?>
                        </span>
                        <span class="rcmds">
                            <?php echo intval(get_post_meta(get_the_ID(), '_recommendations', true)) ?> preporuka
                        </span>
                        <span class="comms">
                            <?php echo intval(get_post_meta(get_the_ID(), '_comments', true)) ?> komentara
                        </span>
                    </div>
                    <div class="the-content">
                        <?php
                        // Article content
                        the_content(); ?>
                    </div>
                    <div class="ender">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tg-logo-red.svg"/>
                    </div>
                    <?php
                    // Article tags
                    the_tags('<div class="tags">', ', ', '</div>'); ?>

                    <?php // Article bottom sidebar
                    dynamic_sidebar('sidebar-single-bottom-1'); ?>

                </div>
            </div>

        </div>
        <div class="navigation">
            <?php previous_post_link(); ?>
            <?php next_post_link(); ?>
        </div>
        <?php
        // Bottom single sidebar - Fullwidth
        dynamic_sidebar('mobile-sidebar-single-bottom-2'); ?>

        <?php
    }
}
?>
    </div>

<?php
get_footer();
