<?php

require_once(__DIR__ . '/../telegram-desktop/functions-shared.php');

add_action( 'wp_enqueue_scripts', 'telegram_enqueue_styles' );
function telegram_enqueue_styles() {
	if ( 'native' === get_post_type() ) {
		wp_enqueue_script( 'jquery');
		wp_enqueue_script( 'slick');
		wp_enqueue_script( 'wow');
		wp_enqueue_style('native-animate');
		if (get_post_meta( get_the_ID(), 'include_styles', true ) ) {

			wp_enqueue_style('telegram-style', get_stylesheet_uri(), array('slick', 'slick-theme'), '95110.87');
			wp_enqueue_script('telegram-script', get_template_directory_uri() . '/assets/js/functions.js', array('jquery', 'slick', 'hammer-time'), '95050.49', true);
		}
	} else {
		wp_enqueue_style('telegram-style', get_stylesheet_uri(), array('slick', 'slick-theme'), '95110.87');
		wp_enqueue_script('telegram-script', get_template_directory_uri() . '/assets/js/functions.js', array('jquery', 'slick', 'hammer-time'), '95050.49', true);
	}
}