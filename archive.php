<?php
get_header(); ?>

    <div class="category-page single-page">
        <div class="category-head">
            <h1>
                <?php echo get_queried_object()->name; ?>
            </h1>
        </div>

        <?php
        // Feed
        global $wp_query;
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post();
                get_template_part( 'templates/articles/article-1' );
                if (4 === $wp_query->current_post) {
	                the_widget('Telegram_Banner_Widget', array('size' => '300x250-2'));
                }
            }
	        next_posts_link('Još Telegrama');
        } ?>



    </div>

<?php
get_footer();