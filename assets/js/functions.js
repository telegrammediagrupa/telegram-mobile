jQuery(document).ready(function() {

    // Navbar
    var iScrollPos = 0;
    jQuery(document).scroll( function() {
        var scrollTop = jQuery(window).scrollTop();

        if( scrollTop >= 350 ) {
            jQuery('nav').addClass('hide');
            var iCurScrollPos = jQuery(this).scrollTop();
                if (iCurScrollPos < iScrollPos) {
                    //Scrolling Up
                    jQuery('nav').addClass('show');
                }
                else if (iCurScrollPos > iScrollPos) {
                    // Scrolling Down
                    jQuery('nav').removeClass('show');
                }
                iScrollPos = iCurScrollPos;
        } else {
            jQuery('nav').removeClass('hide');
        }
    });

    // Left menu
    jQuery('.burger').click( function() {
        jQuery('body').toggleClass('menu-active');
    });


    // Search
    jQuery('.right-action').click( function() {
        jQuery('.global-search').slideDown(200);
    });

    jQuery('.close').click( function() {
        jQuery('.global-search').slideUp(200);
    });


    // Show results AFTER loading bar has loaded
    var feild = jQuery('#search-feild');

    feild.on("change paste keyup", function() {
        function loopBar() {
            jQuery('.loader .bar').animate({left: '100%'}, 2000).animate({left: '-100%'}, 0, loopBar);
        }
        loopBar();
    });


    // [Home] Slick - Procitajte Danas Widget
    jQuery('.procitajte-danas .tg-widget-body').slick({
        autoplay: true,
        dots: true,
        prevArrow: '<i class="fa fa-angle-left slick-arrow slick-prev"></i>',
        nextArrow: '<i class="fa fa-angle-right slick-arrow slick-next"></i>',
    });

    // [Home] Slick - Poster Articles
    jQuery('.poster-articles').slick({
        autoplay: true,
        arrows: false,
        centerMode: true
    });

    // [Home] Slick - TG Autori Widget
    jQuery('.tg-autori .tg-widget-body').slick({
        autoplay: true,
        dots: true,
        arrows: false
    });

    // Photo zoom on Single Articles
    var scroll;
    jQuery('figure').click( function() {
        if(jQuery(this).hasClass('img-frame')) {
            jQuery(this).removeClass('img-frame');
            jQuery('body').animate({
                scrollTop: scroll
            }, 400);
        } else {
            scroll = jQuery(window).scrollTop();
            jQuery(this).addClass('img-frame');
            jQuery(this).scrollLeft(jQuery(this).width());
            jQuery('body').animate({
                scrollTop: jQuery(this).offset().top
            }, 400);
        }
    });


    // Gallery
    if (jQuery('.single-fotogalerije .gallery-slider').length) {
        jQuery('.gallery-slider').slick({
            prevArrow: '.prev-img',
            nextArrow: '.next-img',
            slide: '.gallery-image'
        }).on('afterChange', function (event, slick, currentSlide) {

            jQuery('.gallery-page  .caption').text(gallery_captions[currentSlide].caption);
            jQuery('.gallery-page  .photo-author').text(gallery_captions[currentSlide].foto);
            jQuery('.gallery-page  .slide-num span').text((currentSlide + 1) + '/' + (gallery_captions.length));
            ga('send', 'pageview');
            pp_gemius_hit(tmg_gemius_identifier);
        });
        jQuery('.gallery-page  .caption').text(gallery_captions[0].caption);
        jQuery('.gallery-page  .photo-author').text(gallery_captions[0].foto);
        jQuery('.gallery-page  .slide-num span').text('1/' + (gallery_captions.length));

    }
    if (jQuery('.single-partneri .gallery-slider, .single-price .gallery-slider').length) {
        jQuery('.gallery-slider').slick({
            slide: '.gallery-image'
        }).on('afterChange', function (event, slick, currentSlide) {
            ga('send', 'pageview');
            pp_gemius_hit(tmg_gemius_identifier);
        });
    }

    if (jQuery('.single-fotogalerije .wp-block-gallery').length) {
        jQuery('.wp-block-gallery').slick({
            slide: 'li',
            prevArrow: '.prev-img',
            nextArrow: '.next-img',
        })
            .on('afterChange', function (event, slick, currentSlide) {
                jQuery('.gallery-page  .slide-num span').text((currentSlide + 1) + '/' + (slick.$slides.length));
                ga('send', 'pageview');
                pp_gemius_hit(tmg_gemius_identifier);
            });
    }

    //comments
    jQuery('.comments-activate').click( function() {
            jQuery(this).hide();
            jQuery('.comments-hide').show();
        });

    jQuery('a[target=_blank]').click(function () {
        var url = jQuery(this).attr('href');
        ga('send', 'event', 'outbound', 'click', url, {
            'transport': 'beacon'
        });
    });

});