<?php
get_header(); ?>

<?php
if( have_posts() ) {
    while ( have_posts() ) {
        the_post();
        ?>
        <div class="single-page gallery-page">
            <div class="container">
                <div class="gallery-head">
                    <h3 class="overtitle"><?php the_category(); ?></h3>
                    <h1 class="title">
                        <?php the_title(); ?>
                    </h1>
                    <h2 class="subtitle">
                        <?php
                        echo get_excerpt( get_post_meta( get_the_ID(), 'subtitle', true ) , 141 ); ?>
                    </h2>
                </div>
                <div class="gallery-container">

                    <div class="gallery-nav">
                        <div class="arrow arrow-left prev-img" ><i class="fa fa-angle-left"></i> Prethodna</div>
                        <div class="slide-num">Slika: <span>1/9</span></div>
                        <div class="arrow arrow-right next-img" >Sljedeća <i class="fa fa-angle-right"></i></div>
                    </div>
                    
                    <div>
                        <?php the_content(); ?>
                    </div>

                </div>
            </div>
        </div>

        <?php
    }
}

get_footer();
