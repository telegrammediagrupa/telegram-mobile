<article class="article-rainbow-small article-author">
    <div class="thumb">
        <div class="overlay"></div>
        <?php
        // Thumbsize: 190x120 px
        the_post_thumbnail('article-rainbow-small'); ?>

        <div class="author-block">
            <div class="overlay"></div>
            <?php
            foreach (get_coauthors() as $author) { ?>
                <div class="author-thumb">
                    <?php echo coauthors_get_avatar($author, 120); ?>
                </div>
            <?php } ?>
        </div>

    </div>
    <div class="titles">
        <h3 class="overtitle">
            <?php echo $author->display_name; ?>
        </h3>
        <h1 class="title">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h1>
        <div class="article-meta">
            <span class="rcmds">
                <?php echo intval(get_post_meta(get_the_ID(), '_recommendations', true)) ?> preporuka
            </span>
        </div>
    </div>
</article>