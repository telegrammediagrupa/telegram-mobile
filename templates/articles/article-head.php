<article class="article-head">
    <div class="thumb">
        <?php
        get_template_part('templates/articles/article-badge');
        // Full screen size
        the_post_thumbnail('mobile-head'); ?>
        <div class="overlay"></div>
    </div>
    <div class="article-content">
        <div class="container">

            <div class="titles">
                <?php
                // Author / Time / Recommendations / Comments
                get_template_part('templates/articles/article-meta'); ?>
                <h1 class="title">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h1>
            </div>
        </div>
    </div>
</article>