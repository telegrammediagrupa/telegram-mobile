<div class="article-meta">
    <?php

    $author = get_coauthors();
    if($author) {
        $author = $author[0];
    ?>
    <div class="author-block">
        <div class="author-thumb">
            <a href="<?php echo get_author_posts_url( $author->ID, $author->user_login ); ?>">
                <?php
                if( coauthors_get_avatar($author) ) {
                    echo coauthors_get_avatar($author, 30);
                } else {
                    ?>
                    <div class="initials">
                        <?php
                        echo $author->first_name[0];
                        echo $author->last_name[0]; ?>
                    </div>
                    <?php
                } ?>
            </a>
        </div>
        <span class="author">
            <a href="<?php echo get_author_posts_url( $author->ID, $author->user_login ); ?>">
                <?php echo $author->display_name; ?>
            </a>
        </span>
    </div>
    <?php } ?>
    <span class="time"><?php the_time() ?></span>
    <span class="rcmds"><?php echo intval(get_post_meta(get_the_ID(), '_recommendations', true)) ?> preporuka</span>
    <span class="comms"><?php echo intval(get_post_meta(get_the_ID(), '_comments', true)) ?> komentara</span>
</div>