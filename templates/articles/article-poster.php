<article class="article-poster">
    <a href="<?php echo $link ?>">
        <div class="thumb">
            <?php echo $image ?>
        </div>
    </a>
    <div class="titles ">
        <div class="title-block">
            <h1 class="title">
                <a href="<?php echo $link ?>">
                    <?php
                    echo $title; ?>
                </a>
            </h1>
            <div class="article-meta">
                <span class="rcmds">
                    <?php echo $recommendations ?> preporuka
                </span>
            </div>
        </div>
    </div>
</article>