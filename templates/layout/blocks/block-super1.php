<?php $block = wp_cache_get('super1_block', 'mobile_blocks');
if (!$block) {
ob_start();
?>
<div class="block block-super1">
    <div class="container">
        <div class="tg-widget-head big-title lined">
            Super1
        </div>
        <div class="block-body feed-container">
            <?php
            switch_to_blog( 3 );
            $articles = z_get_zone_query('telegram-homepage', ['post_type' => array( 'post' )]);
            $out = [];
            foreach ($articles->posts as $article) {
	            $out[] = $article->ID;
            }
            $args     = array(
	            'posts_per_page' => 3,
	            'no_found_rows'  => true,
	            'post_type'      => array( 'post' ),
	            'post_status'    => 'publish',
	            'post__not_in' => $out
            );
            $articles2 = new WP_Query( $args );
            global $post;

            if ($articles->have_posts()) {
                while ($articles->have_posts()) {
                    $articles->the_post();
                        get_template_part('templates/articles/article-1');

                    ?>
                    <?php
                } }
            while($articles2->have_posts()) {
                $articles2->the_post();
		            ?>
                    <article class="article-text">
                        <h1 class="title">
                            <a href="<?php the_permalink(); ?>">
					            <?php the_title(); ?>
                            </a>
                        </h1>
                        <p>
	                    <?php echo get_excerpt( get_the_excerpt(), 141 ); ?>
                        </p>
			            <?php
			            // Author / Time / Recommendations / Comments
			            get_template_part('templates/articles/article-meta'); ?>
                    </article>
		            <?php
            }
            wp_reset_postdata();
            restore_current_blog();
            ?>
        </div>
    </div>
</div>
<?php
	$block = ob_get_clean();
	wp_cache_set( 'super1_block', 'mobile_blocks', 15 * 60 );
}

echo $block;
