<div class="left-menu">
    <div class="inner-container">
        <?php $menus = wp_cache_get('sidebar_menus', 'mobile');
            if (!$menus) {
                ob_start();

        ?>
        <div class="menu-section">
            <div class="menu-head">
                Rubrike
            </div>
            <div class="menu-block">
                <?php wp_nav_menu( array('menu'=>'TG Glavni izbornik')); ?>
            </div>
        </div>

        <div class="menu-section">
            <div class="menu-head">
                Kanali
            </div>
            <div class="menu-block">
                <?php wp_nav_menu( array('menu'=>'TG Kanali')); ?>
            </div>
        </div>

        <div class="menu-section">
            <div class="menu-head">
                Social
            </div>
            <div class="menu-block">
                <?php wp_nav_menu( array('menu'=>'TG Društvene mreže')); ?>
            </div>
        </div>

        <div class="menu-section">
            <div class="menu-head">
                Info
            </div>
            <div class="menu-block">
                <?php wp_nav_menu( array('menu'=>'TG Info')); ?>
                <?php /* global $wp;
                $current_url = trailingslashit( home_url( add_query_arg( array(), $wp->request ) ) ); ?>
                <a href="<?php echo esc_url( $current_url . '?ak_action=reject_mobile' ); ?>">Računalna verzija</a>
                */ ?>
            </div>
        </div>
        <?php
                $menus = ob_get_clean();
                wp_cache_set('sidebar_menus', $menus, 'mobile', DAY_IN_SECONDS);
            }

            echo $menus;
        ?>
    </div>
</div>