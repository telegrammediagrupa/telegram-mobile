<div class="megahead">
    <?php

    $articles = z_get_zone_query('head-article', array('posts_per_page' => 1));
    if ( $articles->have_posts() ) {
        while ( $articles->have_posts() ) {
            $articles->the_post();
            get_template_part( 'templates/articles/article-head' );
        }
    }
    //the_widget('Telegram_AndolPro');
    the_widget('Telegram_Najcitanije_Malo');
    ?>
</div>