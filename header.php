<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">

    <!-- Twitter embed box -->
    <meta name="twitter:widgets:theme" content="light">
    <meta name="twitter:widgets:link-color" content="#ed1d39">
    <meta name="twitter:widgets:border-color" content="#eee">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php
	if (is_front_page()) {
		?><meta name="description" content="Pročitajte najnovije vijesti iz Hrvatske i svijeta. Društvene analize, kolumne političkih stručnjaka, velike priče o malim ljudima. Portal bez treša i estrade, za ljude koji razmišljaju o svijetu budućnosti."><?php
	}
	?>
    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="top">
<?php do_action('telegram_body_start'); ?>

<?php
// Top Banner
the_widget('Telegram_Banner_Widget', array('size' => 'telegram_mobile_header')) ?>



<div class="main-container">
    <nav class="nav-small">
        <div class="container">
            <div class="burger left-action">
                <div class="bar bar-1"></div>
                <div class="bar bar-2"></div>
                <div class="bar bar-3"></div>
            </div>
            <div class="search right-action">
                <i class="fa fa-search"></i>
            </div>
            <div class="the-logo">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/telegram_logo.svg" width="770" align="Telegram.hr"/>
                </a>
            </div>
        </div>
        <div class="overlay"></div>
    </nav>

    <?php
    get_template_part('templates/layout/left-menu');
    get_template_part('templates/layout/search-form');