</div> <!-- End Main-Container -->

<?php
// Bottom Banner
the_widget('Telegram_Banner_Widget', array('size' => 'telegram_mobile_footer')) ?>

<footer>
    <div class="container">
        <?php $footer = wp_cache_get('footer', 'mobile');
        if (!$footer) {
            ob_start();

        ?>
        <div class="footer-1">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/telegram_logo.svg" height="70"/>
            <div class="claim">
                Portal za društvena i kulturna pitanja.<br> I svijet koji dolazi.
            </div>
        </div>
        <div class="footer-2">
            <div class="menus">
                <?php wp_nav_menu( array('menu'=>'TG Info')); ?>
                <?php wp_nav_menu( array('menu'=>'TG Telegram Grupa')); ?>
            </div>
        </div>
        <?php /* global $wp;
        $current_url = trailingslashit( home_url( add_query_arg( array(), $wp->request ) ) ); ?>
        <a class="btn btn-empty" href="<?php echo esc_url( $current_url . '?ak_action=reject_mobile' ); ?>">Desktop verzija</a>
        */ ?>
        <div class="meta">
            Sva prava pridržana &copy; <?php the_date('Y'); ?> Telegram Media Grupa d.o.o.
        </div>
        <?php
            $footer = ob_get_clean();
            wp_cache_set('footer', $footer, 'mobile', DAY_IN_SECONDS);
        }
        echo $footer;
        ?>
    </div>
</footer>
<?php
the_widget('Telegram_Banner_Widget', ['size' => 'telegram_sticky']);
wp_footer(); ?>
</body>
</html>