<?php
get_header();

    // Get megahead
    get_template_part('templates/layout/megahead');

    // Feed
    $articles = z_get_zone_query('head-article', array('posts_per_page' => 3, 'offset' => 1));
    if ( $articles->have_posts() ) {
        while ( $articles->have_posts() ) {
            $articles->the_post();
            get_template_part( 'templates/articles/article-1' );
        }
    }
    wp_reset_postdata();

$pos1 = false;
$pos2 = false;
$pos3 = false;
$pos4 = false;

$pos = get_option( 'telegram_promo_mobile' );
shuffle( $pos );
if ( ! empty( $pos ) ) {
	if ( isset( $pos[0] ) ) {
		$pos1 = $pos[0];
	}
	if ( isset( $pos[1] ) ) {
		$pos2 = $pos[1];
	}
	if ( isset( $pos[2] ) ) {
		$pos3 = $pos[2];
	}
	if ( isset( $pos[3] ) ) {
		$pos4 = $pos[3];
	}
}
    global $wpdb;
	$current = 0;
	$sql = "select meta_key, meta_value from wp_postmeta where post_id = %d and ( meta_key in ('_zoninator_order_37783', 'latest_off'))";
	$break = [
		get_post_meta(519214, 'break_1_article', true)[0],
		get_post_meta(519214, 'break_2_article', true)[0]
	];
    while ( have_posts() ) {
    	the_post();
	    if (in_array(get_the_ID(), $break)){
		    continue;
	    }
	    $metas = $wpdb->get_results($wpdb->prepare($sql, get_the_ID()), ARRAY_A);
	    if ($metas) {
		    $should_quit = false;
		    foreach ($metas as $meta) {
			    if ($meta['meta_key'] === 'latest_off') {
				    if ($meta['meta_value'] == 1) {
					    $should_quit = true;
					    break;
				    }
			    }
			    else {
				    $should_quit = true;
				    break;
			    }
		    }
		    if ( $should_quit ) {
			    continue;
		    }
	    }
	    get_template_part( 'templates/articles/article-1' );
	    if ($current == 3) {
		    if ( $pos1 ) {
			    global $post;
			    $p    = get_post( $pos1 );
			    $post = $p;
			    get_template_part( 'templates/articles/article-1' );
		    }
		    wp_reset_postdata();
		    the_widget( 'Telegram_Banner_Widget', array( 'size' => 'telegram_mobile_intext_v1' ) );

		    get_template_part('templates/layout/megabreak');
		    ?>
            <div class="tg-widget-head big-title lined">
                Još važnih vijesti
            </div>
        <?php
	    }

	    if( $current == 5 ) {
		    the_widget('Telegram_Mobile_Autori');
	    }

	    if( $current == 10 ) {
		    if ($pos2) {
			    global $post;
			    $p    = get_post( $pos2 );
			    $post = $p;
			    get_template_part( 'templates/articles/article-1' );
		    }
		    wp_reset_postdata();
		    telegram_load_megabreak('break', 2);
	    }
	    $current++;
	    if ($current === 18) {
		    if ( $pos3 ) {
			    global $post;
			    $p    = get_post( $pos3 );
			    $post = $p;
			    get_template_part( 'templates/articles/article-1' );
		    }
		    if ( $pos4 ) {
			    global $post;
			    $p    = get_post( $pos4 );
			    $post = $p;
			    get_template_part( 'templates/articles/article-1' );
		    }
	    }
	    if ($current > 18) {
	    	break;
	    }
    }

    //TODO: load more

get_footer();
